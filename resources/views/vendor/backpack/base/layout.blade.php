<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('backpack::inc.head')
    
    @if(empty($load_css))
    <!-- do nothing -->
    <!-- <p>Data does not exist</p> -->
    @else
    <!-- loop through the scripts -->
    @foreach($load_css as $css)
    <!-- <script type="text/javascript" src="{{ $css }}"></script> -->
    <link rel="stylesheet" href="{{ $css }}">
    <!-- <p>Your data is here!</p> -->
    @endforeach
    @endif

    @if(empty($style_css))
    <!-- do nothing -->
    <!-- <p>Data does not exist</p> -->
    @else
    <style>
    {{$style_css}}
    </style>
    @endif
</head>

<body class="hold-transition {{ config('backpack.base.skin') }} sidebar-mini">
    <script type="text/javascript">
        /* Recover sidebar state */
        (function () {
            if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
                var body = document.getElementsByTagName('body')[0];
                body.className = body.className + ' sidebar-collapse';
            }
        })();

    </script>
    <!-- Site wrapper -->
    <div class="wrapper">

        @include('backpack::inc.main_header')

        <!-- =============================================== -->

        @include('backpack::inc.sidebar')

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            @yield('header')

            <!-- Main content -->
            <section class="content">

                @yield('content')

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer text-sm clearfix">
            @include('backpack::inc.footer')
        </footer>
    </div>
    <!-- ./wrapper -->


    @yield('before_scripts')
    @stack('before_scripts')

    @include('backpack::inc.scripts')
    @include('backpack::inc.alerts')

    @yield('after_scripts')
    @stack('after_scripts')

    <script>
        /* Store sidebar state */
        $('.sidebar-toggle').click(function (event) {
            event.preventDefault();
            if (Boolean(sessionStorage.getItem('sidebar-toggle-collapsed'))) {
                sessionStorage.setItem('sidebar-toggle-collapsed', '');
            } else {
                sessionStorage.setItem('sidebar-toggle-collapsed', '1');
            }
        });

        // Set active state on menu element
        var full_url = "{{ Request::fullUrl() }}";
        var $navLinks = $("ul.sidebar-menu li a");
        // First look for an exact match including the search string
        var $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href') === full_url;
            }
        );
        // If not found, look for the link that starts with the url
        if (!$curentPageLink.length > 0) {
            $curentPageLink = $navLinks.filter(
                function () {
                    return $(this).attr('href').startsWith(full_url) || full_url.startsWith($(this).attr('href'));
                }
            );
        }

        $curentPageLink.parents('li').addClass('active');

        $(document).ready(function () {
            $('#approved-date-bs').nepaliDatePicker({
                onChange: function () {
                    $('#approved-date-ad').val(BS2AD($('#approved-date-bs').val()));
                }
            });
            $('#approved-date-ad').change(function () {
                $('#approved-date-bs').val(AD2BS($('#approved-date-ad').val()));
            });

            $('#approved-date-bs').change(function () {
                $('#approved-date-ad').val(BS2AD($('#approved-date-bs').val()));
            });


        });

    </script>
    <script type="text/javascript" src="{{ asset('js/nepali.datepicker.v2.2.min.js') }}"></script>
 

    <!-- JavaScripts -->
    {{-- <script src="{{ mix('js/app.js') }}"></script> --}}

    @if(empty($load_scripts))
    <!-- do nothing -->
    <!-- <p>Data does not exist</p> -->
    @else
    <!-- loop through the scripts -->
    @foreach($load_scripts as $script)
    <script type="text/javascript" src="{{ $script }}"></script>
    <!-- <p>Your data is here!</p> -->
    @endforeach
    @endif
    @if(empty($script_js))
    <!-- do nothing -->
    <!-- <p>Data does not exist</p> -->
    @else
    <script>
    {!! html_entity_decode($script_js) !!}
    </script>
    @endif
</body>

</html>
