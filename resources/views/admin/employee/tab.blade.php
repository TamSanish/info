<div class="row">
    <div class='col-md-10 col-md-offset-1'>
        <!-- <div class='nav-tabs-custom'> -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="nav-item disabled active">
                <a class="nav-link" tabindex="-1" href="{{ url($crud->route.'/'.$entry->getKey().'/edit') }}" role="tab">employee</a>
            </li>
            <li role="presentation" class="nav-item">
                <a class="nav-link" href="{{backpack_url('converter')}}" role="tab">converter</a>
            </li>
            <li role="presentation" class="nav-item">
                <a class="nav-link" href="{{backpack_url('district')}}" role="tab">district</a>
            </li>
            <li role="presentation" class="nav-item">
                <a class="nav-link" href="{{backpack_url('country')}}" role="tab">country</a>
            </li>
            
        </ul>
    </div>
</div>
@section('after_scripts')
    <script src="{{ asset('js/press_representative.js') }}"></script>
@endsection 
