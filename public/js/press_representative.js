window.onload = function(){
    // Code. . .
    // $("input#is_nepalese_citizen").change(toggle_citizenship_category);
    $('input:radio[name=is_nepalese_citizen]').change(toggle_citizenship_category);
    $('input:radio[name=is_freelance]').change(toggle_freelance);
    $('input:radio[name=is_renewal]').change(toggle_renewal);
    $("#apply_pass_button").click(self_declaration_form_button_click);
    $("#press-pass-apply").click(self_declaration_list_button_click);
    //$('input:')
    toggle_citizenship_category();
    toggle_freelance();
    nepali_date_converter();
    toggle_renewal();

    $("input:text[name=first_name_en]").change(function(){
        if($("input[name='is_nepalese_citizen']:checked").val() == "0"){
            $("input:text[name=first_name_lc]").val(this.value); 
        }
    });
    $("input:text[name=middle_name_en]").change(function(){
        if($("input[name='is_nepalese_citizen']:checked").val() == "0"){
            $("input:text[name=middle_name_lc]").val(this.value); 
        }
    });
    $("input:text[name=last_name_en]").change(function(){
        if($("input[name='is_nepalese_citizen']:checked").val() == "0"){
            $("input:text[name=last_name_lc]").val(this.value); 
        }
    });
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null) {
           return null;
        }
        return decodeURI(results[1]) || 0;
    }
};
function self_declaration_list_button_click(){
    var pre = document.createElement('pre');
    //custom style.
    pre.style.maxHeight = "500px";
    pre.style.margin = "0";
    pre.style.padding = "24px";
    pre.style.whiteSpace = "pre-wrap";
    pre.style.textAlign = "justify";
    pre.appendChild(document.createTextNode($('#self_declaration_content').text()));
    alertify.defaults.glossary.title = "CMIS:: Confirmation";
    //show as confirm
    alertify.confirm(pre, function(){
        // window.location.replace("http://stackoverflow.com");
        var pass_id = $.urlParam("pl2");
        alertify.success('Thanks for submitting the application. We will keep in touch.');
        // window.location.replace("/app/press_pass_history/"+pass_id+"/apply");
        window.location.replace($("#press-pass-apply").attr("href2"));

        // $("input:hidden[name=pass_application_confirm]").val("yes");
        // $("#saveActions button:submit").click();

    },function(){
        alertify.error('Declined.');
    }).set({labels:{ok:'Accept', cancel: 'Decline'}, padding: false});
}
function self_declaration_form_button_click(){
    var pre = document.createElement('pre');
    //custom style.
    pre.style.maxHeight = "500px";
    pre.style.margin = "0";
    pre.style.padding = "24px";
    pre.style.whiteSpace = "pre-wrap";
    pre.style.textAlign = "justify";
    pre.appendChild(document.createTextNode($('#self_declaration_content').text()));
    alertify.defaults.glossary.title = "CMIS:: Confirmation";
    //show as confirm
    alertify.confirm(pre, function(){
        $("input:hidden[name=pass_application_confirm]").val("yes");
        alertify.success('Thanks for submitting the application. We will keep in touch.');
        $("#saveActions button:submit").click();

    },function(){
        alertify.error('Declined.');
    }).set({labels:{ok:'Accept', cancel: 'Decline'}, padding: false});
}
function nepali_date_converter(){

    $('#pass_issued_date_ad').change(function(){
        $('#pass_issued_date_bs').val(AD2BS($('#pass_issued_date_ad').val()));
    });

    $('#pass_issued_date_bs').change(function(){
        $('#pass_issued_date_ad').val(BS2AD($('#pass_issued_date_bs').val()));
    });
    

    $('#pass_expiry_date_ad').change(function(){
        $('#pass_expiry_date_bs').val(AD2BS($('#pass_expiry_date_ad').val()));
    });

    $('#pass_expiry_date_bs').change(function(){
        $('#pass_expiry_date_ad').val(BS2AD($('#pass_expiry_date_bs').val()));
    });

    $('#dob_date_ad').change(function(){
        $('#dob_date_bs').val(AD2BS($('#dob_date_ad').val()));
    });
    // debugger;
    // $('.nepali-calendar').nepaliDatePicker();
    $('#dob_date_bs').change(function(){
        $('#dob_date_ad').val(BS2AD($('#dob_date_bs').val()));
    });

    $('#citizenship_issue_date_ad').change(function(){
        $('#citizenship_issue_date_bs').val(AD2BS($('#citizenship_issue_date_ad').val()));
    });

    $('#citizenship_issue_date_bs').change(function(){
        $('#citizenship_issue_date_ad').val(BS2AD($('#citizenship_issue_date_bs').val()));
    });

    $('#passport_issue_date_ad').change(function(){
        $('#passport_issue_date_bs').val(AD2BS($('#passport_issue_date_ad').val()));
    });

    $('#passport_issue_date_bs').change(function(){
        $('#passport_issue_date_ad').val(BS2AD($('#passport_issue_date_bs').val()));
    });

    $('#start_date_ad').change(function(){
        $('#start_date_bs').val(AD2BS($('#start_date_ad').val()));
    });
    
    $('#start_date_bs').change(function(){
        $('#start_date_ad').val(BS2AD($('#start_date_bs').val()));
    });
    
    $('#license_issued_date_ad').change(function(){
        $('#license_issued_date_bs').val(AD2BS($('#license_issued_date_ad').val()));
    });
    
    $('#license_issued_date_bs').change(function(){
        $('#license_issued_date_ad').val(BS2AD($('#license_issued_date_bs').val()));
    });
    
    $('#license_renewal_date_ad').change(function(){
        $('#license_renewal_date_bs').val(AD2BS($('#license_renewal_date_ad').val()));
    });
    
    $('#license_renewal_date_bs').change(function(){
        $('#license_renewal_date_ad').val(BS2AD($('#license_renewal_date_bs').val()));
    });
    
    $('#license_expiry_date_ad').change(function(){
        $('#license_expiry_date_bs').val(AD2BS($('#license_expiry_date_ad').val()));
    });
    
    $('#license_expiry_date_bs').change(function(){
        $('#license_expiry_date_ad').val(BS2AD($('#license_expiry_date_bs').val()));
    });
    
    $('#from_date_ad').change(function(){
        $('#from_date_bs').val(AD2BS($('#from_date_ad').val()));
    });
    
    $('#from_date_bs').change(function(){
        $('#from_date_ad').val(BS2AD($('#from_date_bs').val()));
    });

    $('#to_date_ad').change(function(){
        $('#to_date_bs').val(AD2BS($('#to_date_ad').val()));
    });
    
    $('#to_date_bs').change(function(){
        $('#to_date_ad').val(BS2AD($('#to_date_bs').val()));
    });
    
    $('#person_citizenship_date_ad').change(function(){
        $('#person_citizenship_date_bs').val(AD2BS($('#person_citizenship_date_ad').val()));
    });
    
    $('#person_citizenship_date_bs').change(function(){
        $('#person_citizenship_date_ad').val(BS2AD($('#person_citizenship_date_bs').val()));
    });
    
    $('#org_registration_date_ad').change(function(){
        $('#org_registration_date_bs').val(AD2BS($('#org_registration_date_ad').val()));
    });
    
    $('#org_registration_date_bs').change(function(){
        $('#org_registration_date_ad').val(BS2AD($('#org_registration_date_bs').val()));
    });

    $('#issued_date_ad').change(function(){
        $('#issued_date_bs').val(AD2BS($('#issued_date_ad').val()));
    });
    
    $('#issued_date_bs').change(function(){
        $('#issued_date_ad').val(BS2AD($('#issued_date_bs').val()));
    });
    
    $('#valid_date_ad').change(function(){
        $('#valid_date_bs').val(AD2BS($('#valid_date_ad').val()));
    });
    
    $('#valid_date_bs').change(function(){
        $('#valid_date_ad').val(BS2AD($('#valid_date_bs').val()));
    });
    
    $('#completed_year_ad').change(function(){
        $('#completed_year_bs').val(AD2BS($('#completed_year_ad').val()));
    });
    
    $('#completed_year_bs').change(function(){
        $('#completed_year_ad').val(BS2AD($('#completed_year_bs').val()));
    });
    
    $('#start_date_ad').change(function(){
        $('#start_date_bs').val(AD2BS($('#start_date_ad').val()));
    });
    
    $('#start_date_bs').change(function(){
        $('#start_date_ad').val(BS2AD($('#start_date_bs').val()));
    });
    
    $('#end_date_ad').change(function(){
        $('#end_date_bs').val(AD2BS($('#end_date_ad').val()));
    });
    
    $('#end_date_bs').change(function(){
        $('#end_date_ad').val(BS2AD($('#end_date_bs').val()));
    });
    // date_ad
    $('#date_ad').change(function(){
        $('#date_bs').val(AD2BS($('#date_ad').val()));
    });

    $('#date_bs').change(function(){
        $('#date_ad').val(BS2AD($('#date_bs').val()));
    });

}
function toggle_citizenship_category() {
    if ($("input[name='is_nepalese_citizen']:checked").val() == "1") {
        // alert("Allot Thai Gayo Bhai");
        $(".foreign_citizen").hide();
        $(".nepalese_citizen").show();
    }
    if ($("input[name='is_nepalese_citizen']:checked").val() == "0") {
        // alert("Transfer Thai Gayo");
        $(".foreign_citizen").show();
        $(".nepalese_citizen").hide();
    }
}
function toggle_freelance() {
    if ($("input[name='is_freelance']:checked").val() == "1") {
        // alert("Allot Thai Gayo Bhai");
        $(".media_id").hide();
    }
    if ($("input[name='is_freelance']:checked").val() == "0") {
        $(".media_id").show();
    }
}
function toggle_renewal() {
    if ($("input[name='is_renewal']:checked").val() == "1") {
        // alert("Allot Thai Gayo Bhai");
        // $(".pass_number").show();
    }
    if ($("input[name='is_renewal']:checked").val() == "0") {
        // $(".pass_number").hide();
    }
}