{{-- Show the inputs --}}
<?php $fieldsets = $fields->mapToGroups(function ($item, $key) {return [isset($item['fieldset']) ? $item['fieldset'] : 'none' => $item];}); ?>
@foreach ($fieldsets as $fieldset=> $fields)
    <?= $fieldset !== 'none' ? ""
        ."<fieldset class=\"col-md-12\" style=\"border:solid 1px #333!important;padding:0 10px 10px 10px;border-bottom:none;\">"
        ."<legend style=\"width:auto!important;border:none;font-size:15px;font-weight:600;padding:0 10px;\">$fieldset</legend>" : "" ?>
        @foreach($fields as $field)
            @if(view()->exists('vendor.backpack.crud.fields.'.$field['type']))
                @include('vendor.backpack.crud.fields.'.$field['type'], array('field' => $field))
            @else
                @include('crud::fields.'.$field['type'], array('field' => $field))
            @endif
        @endforeach
    <?= $fieldset !== 'none' ? "</fieldset>" : "" ?>
@endforeach