<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('countrys_id');
            $table->integer('present_days');
            $table->integer('absent_days');
            $table->integer('working_days');
            
            
            $table->string('state');
            $table->timestamps();


            $table->foreign('countrys_id','fk_countrys_id')->references('id')->on('countrys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
