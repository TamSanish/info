<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_name')->unique();
            $table->string('caste');
            $table->string('pass_issued_date_bs');
            $table->string('pass_issued_date_ad');
            $table->boolean('mastercard')->nullable()->default(false);
            $table->string('mastercard no',20)->nullable();
            $table->float('pee no')->nullable();
            


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
