<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('District')->unique();
            $table->unsignedSmallInteger('states_id')->nullable;
            $table->unsignedSmallInteger('countrys_id')->nullable;
            $table->timestamps();


            $table->foreign('states_id','fk_states_id')->references('id')->on('states');
            $table->foreign('countrys_id','fk_countrys_id')->references('id')->on('countrys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
