<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\State;

class CountryStateController extends Controller
{

 
    public function index(Request $request)
      {
          $search_term = $request->input('q');
          $form = collect($request->input('form'))->pluck('value', 'name');
          $page = $request->input('page');
          $options = State::query();//model ma query gareko
          // if no category has been selected, show no options
          if (! $form['countrys_id']) {//countryvanne table ma search gareko using id
              return [];
          }
          // if a category has been selected, only show articles in that category
          if ($form['countrys_id']) {
              $options = $options->where('countrys_id', $form['countrys_id']);
          }
          // if a search term has been given, filter results to match the search term
          if ($search_term) {
              $options = $options->where('state', 'LIKE', '%'.$search_term.'%');//k tannalako state ho tesaile
          }
          return $options->paginate(10);
      }
    }
