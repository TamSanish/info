<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ConverterRequest as StoreRequest;
use App\Http\Requests\ConverterRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class ConverterCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ConverterCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        
        $this->crud->setModel('App\Models\Converter');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/converter');
        $this->crud->setEntityNameStrings('converter', 'converters');
        $this->setFields();
        $this->setGridColumns();
        

        
     
        $this->data['list_tab_header_view'] = 'admin.converter.tab';

      
        
      $this->data["load_scripts"] =  array(
            asset('js/randomdateconverter.js')
        );
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
      //  $this->crud->setFromDb();

        // add asterisk for fields that are required in ConverterRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
  



    public function setFields()
    {
        $arr= [
                [
                     'name'=> 'Name',
                     'label'=> 'Name',
                     'type'=>'text',
                ],
                [
                    'name' => 'legend1',
                    'type' => 'custom_html',
                    'value' => '<legend>जन्म मिति </legend>',
                 
                ],

                [
                    'name' => 'current_appointment_date_bs',
                    'label' => 'date_bs',
                    'type' => 'text',
                    'wrapperAttributes' => [
                        'class' => 'form-group col-md-4',
                    ],
                    'attributes' => [
                        'id' => 'current-appointment-date-bs',
                        'maxlength' => '10',
                        'placeholder' => 'yyyy-mm-dd',
                    ]
                ],
                [
                    'name' => 'current_appointment_date_ad',
                    'label' =>'date_ad',
                    'type' => 'date',
                    'wrapperAttributes' => [
                        'class' => 'form-group col-md-4',
                    ],
                    'attributes' => [
                        'id' => 'current-appointment-date-ad'
                    ]
                ],
                [
                    'name' => 'fieldset_open',
                    'type' => 'custom_html',
                    'value' => '</fieldset>'
                ],
                [ // CustomHTML
                    'name' => 'fieldset_open',
                    'type' => 'custom_html',
                    'value' => '<fieldset>',
                ],
                [
                    'name' => 'legend2 ',
                    'type' => 'custom_html',
                    'value' => '<legend>अन्त्य  </legend>',
                 
                ],
     
    ];
               $this->crud->addFields($arr);

    }
    public function setGridColumns()
    {
        $cols= [
                [
                     'name'=> 'Name',
                     'label'=> 'Name',
                
                ],
                [
                    'name' => 'dob_ad',
                    'type' => 'date',
                    'label' => 'dob-ad',
                ],
                [
                    'name' => 'dob_bs',
                    'type' => 'text',
                    'label' => 'dob-bs',
                ],
    
    ];
               $this->crud->addColumns($cols);

    }



    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
    
}
