<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmployeeRequest as StoreRequest;
use App\Http\Requests\EmployeeRequest as UpdateRequest;
use App\Base\DataAccessPermission;
use Backpack\CRUD\CrudPanel;

/**
 * Class EmployeeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class EmployeeCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Employee');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/employee');
        $this->crud->setEntityNameStrings('employee', 'employees');
        $this->setFields();
        $this->setGridcolumns();
        // $this->crud->setEditView('edit');
        $this->data["load_scripts"] =  array(
            asset('js/press_representative.js')
        );



     

         $this->data['edit_tab_header_view'] = 'admin.employee.tab';



      







     


        // $mode = $this->crud->getActionMethod();
        // if ($mode == 'edit' || $mode == 'update') {
        //     // $this->data['recalculate_button'] = true;
        //     $this->setParentLevelParams($this->crud->getCurrentEntryId(), null);
        // } else {
        //     $this->setParentLevelParams(null, null);
        // }

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();

        // add asterisk for fields that are required in EmployeeRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }


 


    private function setFields(){
        $arr =[
            [
            'name'=>'employee_name',
            'type'=> 'text',
            'label'=>'EmployeeName'
            ],
            [
                'name'=>'caste',
                'type'=> 'text',
                'label'=>'caste'
            ],
            [
                'name' => 'legend1',
                'type' => 'custom_html',
                'value' => '<legend>बाकि इन्फो :</legend>',
            ],


            [
                'name'        => 'mastercard', // the name of the db column
                'label'       => 'Have Mastercard ?', // the input label
                'type'        => 'toggle',
                'options'     => [ // the key will be stored in the db, the value will be shown as label; 
                    1 => 'yes',
                    0 => 'No'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ],
                'hide_when' => [ // these fields hide (by name) when the key matches the radio value
                    0 => ['mastercard no', 'pec no'],

                ],
                'inline' => true,
                'default' => 0 // which option to select by default
            ],

            [
                'name' => 'mastercard no',
                'label' => 'mastercard no',
                'type' => 'text',

                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ]
            ],
            [
                'name' => 'pec no',
                'label' => 'pec no',
                'type' => 'number',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ]
            ],
          
            [ // CustomHTML
                'name' => 'fieldset_open',
                'type' => 'custom_html',
                'value' => '<fieldset>',
            ],



            [ 
                'name' => 'pass_issued_date_bs',
                'type' => 'text',
                'label' => 'datesBS',
                'attributes' => [
                    'id' => 'pass_issued_date_bs',
                    'maxlength' => '10',
                    'placeholder'  => 'yyyy-mm-dd',
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3 nepalese_citizen',
                ],
            ],
            [ 
                'name' => 'pass_issued_date_ad',
                'label' => 'dateAd',
                'type' => 'date',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-3',
                ],
                'attributes' => [
                    'id' => 'pass_issued_date_ad',
                    'placeholder'  => 'yyyy-mm-dd',
                ],
            ]









        ];

        $this->crud->addFields($arr);
    }


    private function setGridColumns(){
        $cols=[
            [
                'name'=>'employee_name',
                // 'type'=> 'text',
                'label'=>'EmployeeName'
                ],
                [
                    'name'=>'caste',
                    // 'type'=> 'text',
                    'label'=>'caste'
                ],
                [ 
                    'name' => 'pass_issued_date_bs',
                  
                    'label' => 'dateBs',
                 
                 ],
                  
               
                [ 
                    'name' => 'pass_issued_date_ad',
                    'label' => 'dateAd',
               
             ],
                





        ];
        $this->crud->addColumns($cols);
    }


    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
