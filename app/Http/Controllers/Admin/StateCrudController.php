<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\StateRequest as StoreRequest;
use App\Http\Requests\StateRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class StateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class StateCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\State');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/state');
        $this->crud->setEntityNameStrings('state', 'states');
        $this->setFields();
        $this->setGridColumns();
        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();





        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in StateRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
        
        $this->data['script_js'] = $this->getScriptJs();
    }
    public function getScriptJs(){
        return "
        $(document).ready(function(){
            $('#work-day, #abs-day').on('keyup', function(){
                var val1 = parseInt($('#work-day').val());
                var val2 = parseInt($('#abs-day').val());
                $('#pr-day').val(val1 - val2);

            });
            $('.form-group1').hide();
            $('#to-hide').on('click', function() {
            $('.form-group1').toggle();
       }); 
        });";
    }





    public function setFields()
    {
        $arr =  [
                 [
                'name'=>'state',
                'type'=>'text',
                'label'=>'प्रदेश ',
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-4',
                ],
                 ] ,
                 [
                    'name' => 'countrys_id',
                    'type' => "select2",
                    'label' => 'Name',
                    'entity' => 'countrys',
                    'attribute' => "Name",
                    'model' => "App\Models\Country",

                    
             'options' =>(function ($query){
                return $query->orderBy('Name','ASC')->get();
            }),
                 
            'wrapperAttributes' => [
                'class' => 'form-group col-md-4',
            ],
                ],
                [ // fake attribute
                    'name' => 'fake',
                    'type' => 'toggle_button',
                    'fake' => 'true',
                    'value' => 'अन्य विवरण',
                    'wrapperAttributes' => [
                        'id' => 'to-hide',
                    ],
                ],



                [
                    'name' => 'working_days',
                    'label' => 'workingday',
                    'type' => 'text',
                    'default' => '30',
                    'wrapperAttributes' => [
                        'class' => 'form-group1 col-md-4',
                    ],
                    'attributes' => [
                        'id' => 'work-day',
                        'readonly' => 'readonly',
                    ],
                   
                ],
                [
                    'name' =>  'absent_days',
                    'label' => 'absentday',
                    'type' => 'text',
                    'default' => '0',
                    'wrapperAttributes' => [
                        'class' => 'form-group1 col-md-4',
                    ],
                    'attributes' => [
                        'id' => 'abs-day',
                    ],
                  
                ],
                [
                    'name' =>  'present_days',
                    'label' => 'presentday',
                    'type' => 'text',
                    'default' => '30',
                    'wrapperAttributes' => [
                        'class' => 'form-group1 col-md-4',
                    ],
                    'attributes' => [
                        'id' => 'pr-day',
                        // 'disabled' => 'disabled',
                        'readonly' => 'readonly',
                    ],
                  
    
                ],


            ];
            $this->crud->addFields($arr);
            }
            public function setGridColumns()
    {
        $cols =  [
                 [
                'name'=>'state',
                // 'type'=>'text',
                'label'=>'प्रदेश ',
                 ] ,
                 [
                    'name' => 'countrys_id',
                    'type' => "select",
                    'label' => 'Name',
                    'entity' => 'countrys',
                    'attribute' => "Name",
                    'model' => "App\Models\Country",

                    
            //  'options' =>(function ($query){
            //     return $query->orderBy('Name','ASC')->get();
            // })
                 
                 
                ],
                [
                    'name' => 'working_days',
                    'label' => 'workingday',
                ],
                [
                    'name' => 'absent_days',
                    'label' => 'absentday',
                ],
                [
                    'name' => 'present_days',
                    'label' => 'presentday',
                ],
            ];
            $this->crud->addColumns($cols);
            }






    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
