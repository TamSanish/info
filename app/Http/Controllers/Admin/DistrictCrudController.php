<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DistrictRequest as StoreRequest;
use App\Http\Requests\DistrictRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class DistrictCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DistrictCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\District');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/district');
        $this->crud->setEntityNameStrings('district', 'districts');
        $this->setFields();
        $this->setGridColumns();
        $this->crud->allowAccess('show');
        $this->crud->enableExportButtons();
        $this->crud->allowAccess('revisions');
        $this->crud->with('revisionHistory');
        $this->crud->setEditView('list');

        $this->data['list_tab_header_view'] = 'admin.district.tab';
        

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        // add asterisk for fields that are required in DistrictRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }
    public function setFields()
    {
        $arr =  [
            [
                'name' => 'countrys_id',
                'type' => "select",
                'label' =>'country',
                'model' => "App\Models\Country",
                'entity' => "countrys",
                'attribute' => "Name",
             
            ],
           
          
            //  [
            //     'name' => 'states_id',
            //     'type' => "select2",
            //     'label' =>'state',
            //     'model' => "App\Models\State",
            //     'entity' => "states",
            //     'attribute' => "state",
                   
                
            //      'options' =>(function ($query){
            //     return $query->orderBy('state','ASC')->get();
            // })
             
            // ],
            [
                'name' => 'states_id',//the id from which the states is taken
                'type' => "select2_from_ajax",//method of ajax
                'label' =>'states',//just a label
                'model' => "App\Models\State",//model directory
                'entity' => "states",//relatioship which is inside the model
                'attribute' => "state",//the field which is needed
                'data_source' => url("api/state/countrys_id"),//api/modelsmallname/tableid from which state is taken
                'placeholder' => "Select a state first",
                'minimum_input_length' => 0,
                'dependencies' => ["countrys_id"],//id from which state is pulled
                'wrapperAttributes' => [
                'class' => 'form-group col-md-12',
                ],
            ],
      



         

            [
                'name'=>'District',
                'type'=>'text',
                'label'=>'जिल्ला '
            ],




        ];
    
        

        $this->crud->addFields($arr);
    }

    public function setGridColumns()
    {
        $cols =  [
           
          
             [
                'name' => 'states_id',
                'type' => "select",
                'label' =>'state',
                'model' => "App\Models\State",
                'entity' => "states",
                'attribute' => "state",
                   
                
         
             
            ],

      

            [
                'name'=>'District',
                // 'type'=>'text',
                'label'=>'जिल्ला '
                 ],












        ];

        $this->crud->addColumns($cols);
    }









    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    
}
